package oocl.afs.todolist;

import com.fasterxml.jackson.core.JsonProcessingException;
import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoRepository;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;


@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTests {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private TodoRepository todoRepository;
    @BeforeEach
    void setUp() {
        todoRepository.deleteAll();
    }
    @Test
    void should_find_todos() throws Exception {
        Todo todo = new Todo();
        todo.setText("Study React");
        todoRepository.save(todo);

        mockMvc.perform(get("/todo"))
                .andExpect(status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo.getText()));
    }

    @Test
    void should_add_todo() throws Exception {
        // 创建一个待办事项请求对象
        TodoCreateRequest todoRequest = new TodoCreateRequest();
        todoRequest.setText("Study Java");
        todoRequest.setDone(false);

        // 使用MockMvc模拟HTTP请求，发起POST /todo请求，模拟调用addTodo方法
        mockMvc.perform(post("/todo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(todoRequest)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todoRequest.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoRequest.isDone()));
    }

    @Test
    void should_update_todo() throws Exception {
        // 创建一个待办事项请求对象
        TodoCreateRequest todoRequest = new TodoCreateRequest();
        todoRequest.setText("Study Java");
        todoRequest.setDone(true);

        // 先创建一个待更新的待办事项
        Todo todo = new Todo();
        todo.setText("Study React");
        todo.setDone(false);
        Todo savedTodo = todoRepository.save(todo);

        // 使用MockMvc模拟HTTP请求，发起PUT /todo/{id}请求，模拟调用update方法
        mockMvc.perform(put("/todo/{id}", savedTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(todoRequest)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todoRequest.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoRequest.isDone()));
    }

    private String asJsonString(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void should_delete_todo() throws Exception {
        // 创建一个待办事项
        Todo todo = new Todo();
        todo.setText("Study Java");
        todo.setDone(true);
        Todo savedTodo = todoRepository.save(todo);

        // 使用MockMvc模拟HTTP请求，发起DELETE /todo/{id}请求，模拟调用delete方法
        mockMvc.perform(delete("/todo/{id}", savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200));

        // 验证待办事项是否已被删除
        assertFalse(todoRepository.existsById(savedTodo.getId()));
    }
}
