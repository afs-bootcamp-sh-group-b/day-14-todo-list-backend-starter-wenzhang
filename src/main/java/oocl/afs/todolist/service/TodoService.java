package oocl.afs.todolist.service;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoRepository;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> findAll() {
        return todoRepository.findAll()
                .stream()
                .map(todoEtity -> TodoMapper.toResponse(todoEtity))
                .collect(Collectors.toList());
    }

    public TodoResponse addTodo(TodoCreateRequest todoRequest) {
        Todo todo = new Todo();
        todo.setText(todoRequest.getText());
        todo.setDone(todoRequest.isDone());

        Todo savedTodo = todoRepository.save(todo);
        return TodoMapper.toResponse(savedTodo);
    }

    public TodoResponse updateTodo(Long id, TodoCreateRequest todoRequest) {
        Todo existingTodo = todoRepository.findById(id).orElse(null);
        if (existingTodo != null) {
            existingTodo.setText(todoRequest.getText());
            existingTodo.setDone(todoRequest.isDone());

            Todo updatedTodo = todoRepository.save(existingTodo);
            return TodoMapper.toResponse(updatedTodo);
        }
        return null;
    }

    public boolean deleteTodo(Long id) {
        try {
            todoRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
