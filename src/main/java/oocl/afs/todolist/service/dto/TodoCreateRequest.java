package oocl.afs.todolist.service.dto;

public class TodoCreateRequest {
    private String text;
    private boolean done;

    public TodoCreateRequest() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
