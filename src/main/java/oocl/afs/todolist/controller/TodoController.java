package oocl.afs.todolist.controller;

import oocl.afs.todolist.service.TodoService;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    List<TodoResponse> getAll() {
        return todoService.findAll();
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    TodoResponse create(@RequestBody TodoCreateRequest todoRequest){
        return todoService.addTodo(todoRequest);
    }

    @PutMapping("/{id}")
    TodoResponse update(@PathVariable Long id, @RequestBody TodoCreateRequest todoRequest){
        return todoService.updateTodo(id, todoRequest);
    }

    @DeleteMapping("/{id}")
    boolean delete(@PathVariable Long id){
        return todoService.deleteTodo(id);
    }

}
